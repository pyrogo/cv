---
title: "Valeriano Piromalli"
---

> I was born in Italy, Florence - 09 August 1976
> 
> I’ve worked in Spain, Hong Kong, the Philippines, Cayman Island, Switzerland and Italy.
> 
> I live in Basel, Magnolienpark 9, 4052
> 
> I have a Master in Computer Science - University of Florence - July 2011
> and I'm a CFA Level II Candidate
> 
> I speak Italian, English, and Spanish.
> 
> valerianopiromalli@gmail.com - 0041 79 612 34 98

Corporate Experience
--------------------

<span class="company">Finstar</span>
<span class="location">Lenzburg, Switzerland.</span>

January 2019
: *IT Analyst and DevSecOps Engineer*

I onboard Fintech and Startups on our Open Banking API.
The technical on-boarding pertains to security (OAuth2 Flows) and API usage.

I also cover business requirements collection for API change requests and new features development.

As a DevOps Engineer, I deployed multiple system components, using gitops techniques.
I'm part of the team involved on digital assets and key custody solutions.


-   Flux, Kustomize, k8s templating
-   Java (Spring, Quarkus)

---

<span class="company">Falcon Private Banks</span>
<span class="location">Zurich, Switzerland.</span>

January 2018 - December 2018
: *Senior Full Stack Software Engineer*

I was part of the Digitalization team as a Software Engineer. I've developed and maintained software assets for 
strategic bank innovation. I did customize Avaloq banklets, built REST services to proxy Avaloq SOAP web services, and
developed full-stack, self-made, applications to enrich the digital client touchpoints.

We did use cutting-edge technologies and a modern stack, spanning from API Gateway, Elasticsearch, Ansible, and a
very effective CI/CD platform.

I worked closely daily with our technology partners to guarantee the state-of-the-art integration between our
tailor-made solutions and vendors' digital assets and to establish the integration layer between custom development and
Avaloq proprietary solutions. I was directly involved in architectural and domain decisions regarding our banking and
financial software ecosystem, and I performed devOps operations on a daily based, designed deployment, and building 
processes for our toolchain.

-   Angular, ecma6, sass.
-   Java (Spring).
-   Elasticsearch, Postgres.
-   DevOps: Ansible, Zuul

---

<span class="company">Avaloq</span>
<span class="location">Zurich, Switzerland.</span>

December 2016 - December 2017
: *Senior Software Engineer*

Our team developed a modern CRM used by large financial companies for managing all the various client requests. 

As a front-end developer, I was in charge of developing and maintaining UI components, focusing on the maintainability 
and efficiency of the Angular(1.6) codebase.

A consistent part of my duty was the delivery of solutions for the architect, the business analysts, and the 
requirements engineers.

As a backend developer, I designed and implemented web services (RESTful and SOAP).

I've also developed a consistent number of automated E2E tests using Selenium.

-   Angular, ecma6, sass.
-   Java (EE).
-   Selenium, Jenkins

---

<span class="company">Cognizant</span>
<span class="location">Zurich, Switzerland.</span>

November 2015 - November 2016
: *Senior Technical Associate - Lead Engineer*

I worked as a Lead Engineer for one of the company's largest clients, a multi-national financial services holding company.

As a lead engineer, I was in charge of designing and developing a web application for assisting relationship managers 
in advising their clients regarding their future wealth.

I was the mentor and coordinator for a small off-shore team.

The application had a Java EE web service publishing the REST API (via Jersey), while the client is a Single Page 
Application using Angular and ECMA6.

-   Angular, ecma6.
-   Java EE, Jersey.

---

October 2014 - November 2015
: *Senior Analyst and Project Manager*

<span class="company">Private Asset Management company</span>
<span class="location">Zurich, Switzerland.</span>

The firm is a Wealth Management Company for private investors.

I have developed an in-house Portfolio Management System (PMS) solution. The application helps the Portfolio Manager in
all the common aspects of portfolio management, like portfolio setup and allocation, performance evaluation, risk 
profiling and, trades-related operations.

The system is a classic client-server architecture,
the server providing the public API and an Angular Single Page application, hosted on a Node.js server, consumes the 
RESTful web services.

As a financial analyst, I've provided quantitative analysis of financial instruments, especially fixed income securities.

The rationale concerned the historical prices, the credit and rating risk,
the interest rate premium and macro analysis regarding the country risk and the underlying asset.

-   Angular, Bootstrap, HTML5, CSS3, D3.js.
-   Java (Spring), Node.js, Express.js.
-   MySQL, Redis, MongoDB.

---

April 2014 - October 2014<br/>
: *Senior Developer*

<span class="company">Fincons Group</span>
<span class="location">Rome, Italy.</span>

I was engaged in a project for developing a real-time application.

The developed widget communicates with a CRM platform and provides a modern way to engage the operator, is fully 
independent, embeddable, and based on WebSockets protocol.

I've also provided the server-side Java integration for the component.

-   Pure Object Oriented Javascript, Require.js, CoffeeScript.
-   Node.js, Java (Spring), Ejabberd (XMPP Server).
-   Redis.

---

March 2012 - March 2014
:   *Developer*

<span class="company">Freelance</span>
<span class="location">Florence, Italy.</span>

As a freelance developer I have developed multiple websites
and eCommerce using Magento, Joomla and WordPress.

-   PHP, HTML5, jQuery, CSS.

---

January 2010 - January 2014
:   *Founding Member*

<span class="company">Terrarossa</span>
<span class="location">Buggiano (PT), Italy.</span>

Involved in management and human resource.

----

January 2009 - January 2014
:   *Founding Member*

<span class="company">Mediacoop</span>
<span class="location">Buggiano (PT), Italy.</span>

Management, HR end web projects PM.

----

August 2011 - March 2012
:   *Senior specialist & quantitative analyst* <br/>

<span class="company">FFMiravite</span>
<span class="location">Manila, Philippines.</span>

I covered the role of analyst for quantitative analysis and risk modeling.

I was involved in developing new strategies for the firm while building an
Investment Performance Study of a pension fund and portfolio mix risk-related metrics.

During my experience, I've developed a portfolio allocation system
based on the clustering of an assets' universe composed of multiple international indices.

The effective allocation efficient-frontier is based on mean-variance theory (Markowitz), and the application had a 
stop-loss and early warning system based on VaR computation of the portfolio composition.

-   jQuery, HTML, CSS. 
-   PHP, Java.
-   Matlab.

----

May 2008 - January 2011
:   *Consultant*

<span class="company">FFMiravite</span>
<span class="location">Florence, Italy.</span>

For the firm, I developed a portfolio optimization system and forecasting.

I performed studies on volatility and interest rate, VaR computation, and fat-tail distribution.

Associated with professor Vincenzo Vespri, teacher of Financial Mathematics at University of Florence, Science and Math.

-   Java, Matlab.

----

October 2007 - February 2008
: *Developer and Debugger*

<span class="company">PlanSoft</span>
<span class="location">Italy, Florence</span>

I was engaged as a developer PlanSoft group and contractor for Eli Lilly industry.

My team was in charge of fixing web applications written in ColdFusion and Java.

The team was also involved in testing and refactoring existing code.

-   Coldfusion ,Java.
-   Oracle DB.

---

January 2006 - February 2008
: *Developer*

<span class="company">Freelance</span>
<span class="location">Italy, Florence</span>

Employed as a web developer for Italian and European Companies.

The projects were multiple and spanning from Joomla Integrations to custom CMS and eLearning systems.

-   HTML, CSS, jQuery.
-   PHP.
-   MySQL.

---

January 2003 - February 2005
: *Developer*

<span class="company">OIC</span>
<span class="location">Italy, Florence</span>

During this working experience, I developed websites of the medical seminars, around 20 seminars yearly, with attention 
to the web semantic and design.

-   HTML, CSS, jQuery.
-   PHP.
-   MySQL.

---

January 2004 - March 2004
: *Teacher*

<span class="company">CSL Toscana</span>
<span class="location">Arezzo, Italy</span>

I was the teacher for courses regarding Web Development.

I taught development techniques server-side like PHP and data persistence using MySQL,
and client-side (X)HTML, JavaScript, CSS, together with AJAX related technologies.

---

February 2003 - March 2004
: *Teacher*

<span class="company">Intersistemi Italia S.p.A</span>
<span class="location">Italy</span>

The Company developed the software for the electronic protocol use by
Ministero Dei Beni Culturali of Italy.

I assisted the users employed at Ministry offices in Northern and Central Italy while processing electronic protocol 
using the developed software.
