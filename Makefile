.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

lint: ## Execute lint checks. It requires remark to be installed. Use npm install
	npm run lint-md

html: ## Convert to HTML. Pandoc required
	pandoc -s README.md --css style.css -o valerianopiromalli.html

pdf: ## Convert to PDF. Pandoc required
	pandoc README.md -o valerianopiromalli.pdf

docx: ## Convert to DOC. Pandoc required
	pandoc README.md  -o valerianopiromalli.docx

all: html pdf docx ## Run all the conversion
	